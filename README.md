## Full Stack Developer Technical Task
Created with NestJS + React.

## How to run it?
```bash
$ docker-compose up -d
```
## Default URLs

frontend: http://localhost:3000
backend: http://localhost:3001

 Author - **Jorge Requena** - contact@jorgerequena.com