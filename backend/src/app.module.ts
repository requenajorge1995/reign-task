import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticleModule } from './article/article.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://mongo:27017/reign'),
    ScheduleModule.forRoot(),
    ArticleModule,
  ],
})
export class AppModule {}
