import { Injectable, Logger } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Article, ArticleDocument } from './article.schema';
import { fetchArticles } from './article-fetcher.utils';

@Injectable()
export class ArticlesFetcherService {
  private static readonly TIME_INTERVAL = 60 * 60 * 1000;
  private counter = 0;

  private readonly logger = new Logger(ArticlesFetcherService.name);

  constructor(
    @InjectModel(Article.name)
    private readonly articleModel: Model<ArticleDocument>,
  ) {
    this.fetchArticles();
  }

  @Interval(ArticlesFetcherService.TIME_INTERVAL)
  async fetchArticles() {
    this.counter++;
    try {
      const articles = await fetchArticles();

      await Promise.all(
        articles.map((article) => {
          const query = { _id: article._id };
          const options = { upsert: true };
          return this.articleModel.findByIdAndUpdate(query, article, options);
        }),
      );

      this.logger.log(
        `${this.counter}) ${articles.length} articles fetched successfully`,
      );
    } catch (error) {
      this.logger.error(error.message, error.stack);
    } finally {
      this.logger.log(
        `Sleeping for ${ArticlesFetcherService.TIME_INTERVAL / 1000} seconds`,
      );
    }
  }
}
