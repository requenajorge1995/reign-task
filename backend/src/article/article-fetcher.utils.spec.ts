import {
  removeDuplicateArticles,
  removeIncompleteArticles,
} from './article-fetcher.utils';

describe('Article utils', () => {
  it('should remove duplicates', () => {
    const input = [{ _id: 123 }, { _id: 321 }, { _id: 123 }];

    const output = removeDuplicateArticles(input);

    expect(output).toStrictEqual([{ _id: 123 }, { _id: 321 }]);
  });

  it('should remove incomplete articles', () => {
    const article = {
      _id: 456,
      title: 'a',
      author: 'b',
      url: 'd',
      createdAt: new Date(),
    };

    const input = [
      { _id: 123, author: null },
      { _id: 321, title: null },
      article,
    ];

    const output = removeIncompleteArticles(input);

    expect(output).toStrictEqual([article]);
  });
});
