import axios from 'axios';

export async function fetchArticles() {
  const response = await getArticlesFromHackerNews();

  const parsedArticles = parseHackerNewsResponse(response);
  const completeArticles = removeDuplicateArticles(parsedArticles);
  const uniqueArticles = removeIncompleteArticles(completeArticles);

  return uniqueArticles;
}

export async function getArticlesFromHackerNews(): Promise<HackerNewsResponse> {
  const url = 'https://hn.algolia.com/api/v1/search_by_date';
  const params = {
    query: 'nodejs',
    page: 0,
    hitsPerPage: 500,
  };

  const response = await axios.get<HackerNewsResponse>(url, { params });
  return response.data;
}

export function parseHackerNewsResponse(
  response: HackerNewsResponse,
): PartialArticle[] {
  return response.hits.map((hit) => {
    const { story_title, title, story_url, url, author, created_at, story_id } =
      hit;

    return {
      _id: story_id,
      title: story_title || title,
      author: author,
      url: story_url || url,
      createdAt: created_at ? new Date(created_at) : null,
    };
  });
}

export function removeIncompleteArticles(
  articles: PartialArticle[],
): Article[] {
  return articles.filter((article) =>
    Object.values(article).every((prop) => !!prop),
  ) as Article[];
}

export function removeDuplicateArticles(
  articles: PartialArticle[],
): PartialArticle[] {
  const map: Record<string, PartialArticle> = {};

  articles.forEach((article) => {
    map[article._id] = article;
  });

  return Object.values(map);
}

type PartialArticle = Partial<Article>;

type Article = {
  _id: number;
  title: string;
  author: string;
  url: string;
  createdAt: Date;
};

type HackerNewsResponse = {
  hits: Hit[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  exhaustiveTypo: boolean;
  query: Query;
  params: string;
  renderingContent: any;
  processingTimeMS: number;
};

type Hit = {
  created_at: string;
  title: null | string;
  url: null | string;
  author: string;
  points: number | null;
  story_text: null | string;
  comment_text: null | string;
  num_comments: number | null;
  story_id: number | null;
  story_title: null | string;
  story_url: null | string;
  parent_id: number | null;
  created_at_i: number;
  _tags: string[];
  objectID: string;
  _highlightResult: HighlightResult;
};

type HighlightResult = {
  author: HighlightData;
  comment_text?: HighlightData;
  story_title?: HighlightData;
  story_url?: HighlightData;
  title?: HighlightData;
  url?: HighlightData;
  story_text?: HighlightData;
};

type HighlightData = {
  value: string;
  matchLevel: MatchLevel;
  matchedWords: Query[];
  fullyHighlighted?: boolean;
};

enum MatchLevel {
  Full = 'full',
  None = 'none',
}

enum Query {
  Nodejs = 'nodejs',
}
