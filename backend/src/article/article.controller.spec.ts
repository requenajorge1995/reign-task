import { getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { ArticleController } from './article.controller';
import { Article } from './article.schema';
import { ArticleService } from './article.service';
import { Model } from 'mongoose';

describe('ArticleController', () => {
  let articleController: ArticleController;
  let articleService: ArticleService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [ArticleController],
      providers: [
        ArticleService,
        {
          provide: getModelToken(Article.name),
          useValue: Model,
        },
      ],
    }).compile();

    articleService = moduleRef.get<ArticleService>(ArticleService);
    articleController = moduleRef.get<ArticleController>(ArticleController);
  });

  describe('findAllActive', () => {
    it('should return articles and pagination params', async () => {
      const result = { articles: [], page: 1, limit: 10, total: 0 };

      jest
        .spyOn(articleService, 'findAllActive')
        .mockImplementation(() => Promise.resolve(result));

      expect(await articleController.findAllActive()).toBe(result);
    });
  });

  describe('deactivate', () => {
    it('should deactivate an article', async () => {
      const spy = jest
        .spyOn(articleService, 'deactivate')
        .mockImplementation(() => Promise.resolve());

      expect(await articleController.deactivate(1)).toBeUndefined();
      expect(spy).toBeCalled();
    });
  });
});
