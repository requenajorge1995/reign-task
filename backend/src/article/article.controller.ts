import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Put,
  Query,
} from '@nestjs/common';
import { ArticleService } from './article.service';

@Controller('article')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  findAllActive(
    @Query('page', ParseIntPipe) page = 1,
    @Query('limit', ParseIntPipe) limit = 20,
  ) {
    return this.articleService.findAllActive(page, limit);
  }

  @Put('deactivate/:id')
  deactivate(@Param('id', ParseIntPipe) id: number) {
    return this.articleService.deactivate(id);
  }
}
