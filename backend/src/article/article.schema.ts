import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop()
  _id: number;

  @Prop()
  title: string;

  @Prop()
  author: string;

  @Prop()
  url: string;

  @Prop()
  createdAt: Date;

  @Prop({ default: true })
  active: boolean;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
