import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Article, ArticleDocument } from './article.schema';
import { ArticleService } from './article.service';
import { Model, Query } from 'mongoose';

describe('ArticleService', () => {
  let service: ArticleService;
  let model: Model<ArticleDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getModelToken(Article.name),
          useValue: Model,
        },
        ArticleService,
      ],
    }).compile();

    model = module.get<Model<ArticleDocument>>(getModelToken(Article.name));
    service = module.get<ArticleService>(ArticleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return articles list', async () => {
    const query = new Query();
    jest.spyOn(query, 'exec').mockResolvedValue([]);
    jest.spyOn(model, 'find').mockReturnValue(query as any);
    jest.spyOn(model, 'countDocuments').mockResolvedValue(101);

    const { articles, limit, page, total } = await service.findAllActive(1, 10);

    expect(page).toBe(1);
    expect(limit).toBe(10);
    expect(total).toBe(101);
    expect(articles).toStrictEqual([]);
  });

  it('should deactivate an article', async () => {
    const spy = jest.spyOn(model, 'findByIdAndUpdate').mockResolvedValue(null);

    await service.deactivate(1);

    expect(spy).toBeCalled();
  });
});
