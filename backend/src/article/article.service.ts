import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Article, ArticleDocument } from './article.schema';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel(Article.name)
    private readonly articleModel: Model<ArticleDocument>,
  ) {}

  async findAllActive(
    page: number,
    limit: number,
  ): Promise<{ articles: Article[] } & PaginationProps> {
    const query = { active: true };
    const total = await this.articleModel.countDocuments(query);
    const articles = await this.articleModel
      .find(query)
      .sort({ createdAt: 'desc' })
      .skip((page - 1) * limit)
      .limit(limit)
      .exec();

    return { articles, page, limit, total };
  }

  async deactivate(id: number): Promise<void> {
    await this.articleModel.findByIdAndUpdate({ _id: id }, { active: false });
  }
}

type PaginationProps = {
  page: number;
  limit: number;
  total: number;
};
