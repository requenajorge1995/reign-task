import ArticlesSection from './components/ArticlesSection';
import Header from './components/Header';
import GlobalStyles from './styles/GlobalStyles';

function App() {
  return (
    <>
      <GlobalStyles />
      <Header />
      <ArticlesSection />
    </>
  );
}

export default App;
