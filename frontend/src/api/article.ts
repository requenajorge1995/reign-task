const BASE_URL = 'http://localhost:3001/article';

export async function getArticles(page: number, limit: number) {
  const url = BASE_URL + `?page=${page}&limit=${limit}`;

  const response = await fetch(url);
  return response.json();
}

export async function deactivateArticle(id: number) {
  const url = BASE_URL + '/deactivate/' + id;

  return fetch(url, { method: 'PUT' });
}
