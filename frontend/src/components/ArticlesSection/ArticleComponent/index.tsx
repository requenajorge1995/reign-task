import { Article } from '../../../types/article';
import {
  Container,
  Title,
  Author,
  CreatedAt,
  DeleteButton,
  CreateAtAndDeleteButtonWrapper,
} from './styles';
import { BsFillTrashFill } from 'react-icons/bs';
import { dateResolver } from './utils';

function ArticleComponent({ article, handleRemove }: Props) {
  const { title, author, createdAt, url } = article;

  const onRemove = (e: React.MouseEvent) => {
    e.preventDefault();
    handleRemove();
  };

  return (
    <Container target='_blank' rel='noreferrer' href={url}>
      <Title>
        {title}.
        <Author> - {author} - </Author>
      </Title>
      <CreateAtAndDeleteButtonWrapper>
        <CreatedAt>{dateResolver(createdAt)}</CreatedAt>
        <DeleteButton onClick={onRemove} className='delete-button'>
          <BsFillTrashFill />
        </DeleteButton>
      </CreateAtAndDeleteButtonWrapper>
    </Container>
  );
}

type Props = {
  article: Article;
  handleRemove(): void;
};

export default ArticleComponent;
