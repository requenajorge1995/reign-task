import styled from 'styled-components';

export const Container = styled.a`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #fff;
  text-decoration: none;
  height: 3rem;
  padding: 0 1rem;

  &:hover {
    background-color: #fafafa;

    & .delete-button {
      display: flex;
    }
  }
`;

export const Title = styled.p`
  color: #333;
  font-size: 13px;
`;

export const Author = styled.span`
  color: #999;
`;

export const CreatedAt = styled(Title)``;

export const DeleteButton = styled.div`
  display: none;
  align-items: inherit;
  justify-content: center;
  height: 100%;
  width: 2rem;
  color: gray;
`;

export const CreateAtAndDeleteButtonWrapper = styled.div`
  display: inherit;
  justify-content: inherit;
  align-items: inherit;
  height: 100%;
  width: 7rem;
`;
