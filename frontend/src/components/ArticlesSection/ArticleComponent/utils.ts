import { format, isToday, isYesterday } from 'date-fns';

export function dateResolver(date: Date): string {
  switch (true) {
    case isToday(date):
      return format(date, 'hh:mm aaa');
    case isYesterday(date):
      return 'Yesterday';
    default:
      return format(date, 'MMM d');
  }
}
