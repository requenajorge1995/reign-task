import ErrorMessage from '../ErrorMessage';
import BackdropLoader from '../Loaders/BackdropLoader';
import LinearLoader from '../Loaders/LinearLoader';
import RegularLoader from '../Loaders/RegularLoader';
import ArticleComponent from './ArticleComponent';
import { ArticlesWrapper } from './styles';
import useArticlesSection from './useArticlesSection';

function ArticlesSection() {
  const {
    articles,
    mainLoading,
    deletingLoading,
    gettingMoreLoading,
    error,
    handleRemoveArticle,
    bottomRef,
  } = useArticlesSection();

  if (error) return <ErrorMessage message={error.message} />;

  if (mainLoading) return <LinearLoader />;

  return (
    <>
      {deletingLoading && <BackdropLoader />}
      <ArticlesWrapper>
        {articles.map((article) => (
          <ArticleComponent
            key={article._id}
            article={article}
            handleRemove={() => handleRemoveArticle(article._id)}
          />
        ))}
      </ArticlesWrapper>
      <div ref={bottomRef} />
      {gettingMoreLoading && <RegularLoader />}
    </>
  );
}

export default ArticlesSection;
