import styled from 'styled-components';

export const ArticlesWrapper = styled.div`
  padding: 2rem;

  & > *:not(:last-child) {
    border-bottom: 1px solid #ccc;
  }
`;
