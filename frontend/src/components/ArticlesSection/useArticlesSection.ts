import { useCallback, useEffect, useState } from 'react';
import { getArticles, deactivateArticle } from '../../api/article';
import { Article } from '../../types/article';
import { useInView } from 'react-intersection-observer';

const LIMIT = 20;

function useArticlesSection() {
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [mainLoading, setMainLoading] = useState(true);
  const [deletingLoading, setDeletingLoading] = useState(false);
  const [gettingMoreLoading, setGettingMoreLoading] = useState(false);
  const { ref: bottomRef, inView: bottomReached } = useInView();

  const [error, setError] = useState<Error>();
  const [articles, setArticles] = useState<Article[]>([]);

  const [deletedArticlesQuantity, setDeletedArticlesQuantity] = useState(0);

  const firstLoad = useCallback(() => {
    setMainLoading(true);
    getArticles(1, LIMIT)
      .then(({ articles, total }) => {
        setArticles(parseArticles(articles));
        setTotal(total);
      })
      .catch(setError)
      .finally(() => setMainLoading(false));
  }, [setMainLoading, setArticles, setTotal]);

  //first data fetching
  useEffect(() => {
    firstLoad();
  }, [firstLoad]);

  //loading more articles
  useEffect(() => {
    const shouldLoadMore =
      !gettingMoreLoading && articles.length < total && bottomReached;

    if (!shouldLoadMore) return;

    if (deletedArticlesQuantity >= LIMIT) {
      setDeletedArticlesQuantity(0);
      firstLoad();
      return;
    }

    setGettingMoreLoading(true);
    getArticles(page + 1, LIMIT - deletedArticlesQuantity)
      .then(({ articles, total }) => {
        setArticles((prev) => [...prev, ...parseArticles(articles)]);
        setTotal(total);
        setPage((prev) => prev + 1);
      })
      .catch(setError)
      .finally(() => setGettingMoreLoading(false));
  }, [
    deletedArticlesQuantity,
    gettingMoreLoading,
    articles,
    total,
    bottomReached,
    setGettingMoreLoading,
    setDeletedArticlesQuantity,
    page,
    firstLoad,
  ]);

  const handleRemoveArticle = async (id: number) => {
    setDeletingLoading(true);
    try {
      await deactivateArticle(id);
      setArticles((prev) => prev.filter((article) => article._id !== id));
      setDeletedArticlesQuantity((prev) => prev + 1);
      setTotal((prev) => prev - 1);
    } catch (error) {
      setError(error as Error);
    } finally {
      setDeletingLoading(false);
    }
  };

  return {
    articles,
    error,
    mainLoading,
    deletingLoading,
    gettingMoreLoading,
    handleRemoveArticle,
    bottomRef,
  };
}

function parseArticles(articles: Article[]) {
  return articles.map((article) => ({
    ...article,
    createdAt: new Date(article.createdAt),
  }));
}

export default useArticlesSection;
