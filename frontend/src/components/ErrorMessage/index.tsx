import { Container, Title, Message } from './styles';

function ErrorMessage({ title, message }: Props) {
  return (
    <Container>
      <Title>{title || 'Error'}</Title>
      <Message>{message}</Message>
    </Container>
  );
}

type Props = {
  title?: string;
  message: string;
};

export default ErrorMessage;
