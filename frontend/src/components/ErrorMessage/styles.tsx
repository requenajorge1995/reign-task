import styled from 'styled-components';

export const Container = styled.div`
  margin: 4rem auto;
  padding: 1rem 2rem;
  width: fit-content;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  border-radius: 6px;
  background-color: lightcoral;
`;

export const Title = styled.h4`
  font-size: 24px;
  color: white;
`;

export const Message = styled.p`
  color: black;
`;
