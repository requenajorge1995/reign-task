import { Container, Title, Subtitle } from './styles';

function Header() {
  return (
    <Container>
      <Title>HN Feed</Title>
      <Subtitle>We {'<3'} hacker news!</Subtitle>
    </Container>
  );
}

export default Header;
