import styled from 'styled-components';

export const Container = styled.header`
  background-color: #57939c;
  color: white;
  padding: 2rem;
`;

export const Title = styled.h1`
  font-size: 75px;
`;

export const Subtitle = styled.h2``;
