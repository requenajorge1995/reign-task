import { Container, Loader } from './styles';

function LinearLoader() {
  return (
    <Container>
      <Loader />
    </Container>
  );
}

export default LinearLoader;
