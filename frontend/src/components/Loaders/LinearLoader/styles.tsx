import styled from 'styled-components';

export const Container = styled.div`
  overflow: hidden;
  width: 100%;
  height: 5px;
  background-color: #b3e5fc;
`;

export const Loader = styled.div`
  position: relative;
  width: 100%;
  height: 100%;

  &:before {
    content: '';
    position: absolute;
    height: 100%;
    background-color: #71bbc7;
    animation: indeterminate_first 1.5s infinite ease-out;
  }

  &:after {
    content: '';
    position: absolute;
    height: 100%;
    background-color: #60a2ac;
    animation: indeterminate_second 1.5s infinite ease-in;
  }

  @keyframes indeterminate_first {
    0% {
      left: -100%;
      width: 100%;
    }
    100% {
      left: 100%;
      width: 10%;
    }
  }

  @keyframes indeterminate_second {
    0% {
      left: -150%;
      width: 100%;
    }
    100% {
      left: 100%;
      width: 10%;
    }
  }
`;
