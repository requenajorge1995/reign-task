import { Container, Loader } from './styles';

function RegularLoader() {
  return (
    <Container>
      <Loader>
        <div></div>
        <div></div>
        <div></div>
      </Loader>
    </Container>
  );
}

export default RegularLoader;
