export type Article = {
  _id: number;
  title: string;
  author: string;
  url: string;
  createdAt: Date;
}